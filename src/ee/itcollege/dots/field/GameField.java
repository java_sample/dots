package ee.itcollege.dots.field;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import ee.itcollege.dots.parts.Dot;
import ee.itcollege.dots.parts.Line;

/**
 * This is for graphics
 */
@SuppressWarnings("serial")
public class GameField extends JPanel {
    
    private static final int FIELD_HEIGHT = 10;
    private static final int FIELD_WIDTH = 10;
    
    private final TopPanel top;
    private List<Dot> dots = new ArrayList<Dot>();
    private Line selection = new Line();
    
    public GameField(TopPanel top) {
    	
    	this.top = top;
    	
    	setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    	createDots();
    	this.addMouseMotionListener(new MouseMotionAdapter() {
    		@Override
    		public void mouseDragged(MouseEvent e) {
    			Dot dot = findDot(e);
				if (null != dot) {
					selection.nextDot(dot);
				}
				GameField.this.repaint();
    		}
		});
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				Dot dot = findDot(e);
				if (null != dot) {
					selection.nextDot(dot);
				}
				GameField.this.repaint();
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// remove selected dots if 2 or more was selected
				List<Dot> selectedDots = selection.getSelectedDots();
				if (selectedDots.size() > 1) {
					removeDots(selectedDots);
					GameField.this.top.addPoints(selectedDots.size());
				}
				
				selection.clearSelection();
				GameField.this.repaint();
			}
		});
	}
    
    private Dot findDot(int x, int y) {
    	for (Dot dot : dots) {
			if (x == dot.getX() && y == dot.getY()) {
				return dot;
			}
		}
    	return null;
    }
    
    private Dot findDot(MouseEvent e) {
    	
		int x = e.getX() / Dot.DOT_AREA_SIZE;
		int y = e.getY() / Dot.DOT_AREA_SIZE;
		
    	return findDot(x, y);
    }
    
    private void createDots() {
    	for (int x = 0; x < FIELD_WIDTH; x++) {
			for (int y = 0; y < FIELD_HEIGHT; y++) {
				dots.add(new Dot(x, y));
			}
		}
    }
    
    private void removeDots(List<Dot> selectedDots) {
    	dots.removeAll(selectedDots);
    	
    	int[][] moves = new int[FIELD_HEIGHT][FIELD_WIDTH];
    	
    	// move dots down
    	for (Dot removedDot : selectedDots) {
			for (Dot dot : dots) {
				if (dot.getX() == removedDot.getX() && dot.getY() < removedDot.getY()) {
					moves[dot.getY()][dot.getX()]++;
				}
			}
		}
    	
	    for (int y = 0; y < moves.length; y++) {
			for (int x = 0; x < moves[y].length; x++) {
				int count = moves[y][x];
				if (count > 0) {
					Dot dot = findDot(x, y);
					for (int i = 0; i < count; i++) {
						dot.moveDown();
					}
				}
			}
		}
	    
    	// add dots to column
	    int[] colMoveCount = new int[FIELD_WIDTH];
	    for (Dot dot : selectedDots) {
	    	colMoveCount[dot.getX()]++;
	    }
    	for (int x = 0; x < colMoveCount.length; x++) {
			for (int y = 0; y < colMoveCount[x]; y++) {
				dots.add(new Dot(x, y));
			}
    	}
    }
    
    @Override
    public Dimension getPreferredSize() {
		return new Dimension(FIELD_WIDTH * Dot.DOT_AREA_SIZE, FIELD_HEIGHT * Dot.DOT_AREA_SIZE);
    }
    
    @Override
    protected void paintComponent(Graphics g1) {
    	
    	Graphics2D g = (Graphics2D) g1;
    	
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        g.setColor(Color.white);
        g.fillRect(0, 0, getWidth(), getHeight());
        
        // draw dots
        for (Dot dot : dots) {
			dot.drawItself(g);
		}
        
        selection.drawItself(g);
    }
    
}










