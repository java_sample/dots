package ee.itcollege.dots.field;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class TopPanel extends JPanel {
	
	int points = 0;
	int seconds = 60;

	Label pointsLabel = new Label("points");
	Label timeLabel = new Label("time");
	JButton button = new JButton("Cool!");
	
	public TopPanel() {
		setLayout(new BorderLayout());
		
		Font font = new Font(Font.MONOSPACED, Font.BOLD, 30);
		
		pointsLabel.setFont(font);
		timeLabel.setFont(font);
		
		add(pointsLabel, BorderLayout.WEST);
		add(timeLabel, BorderLayout.EAST);
		add(button, BorderLayout.CENTER);
		
		Timer timer = new Timer(1000, new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	seconds--;
		        timeLabel.setText(String.valueOf(seconds));
		    }
		});
		timer.start();
	}
	
	public void addPoints(int point) {
		points = points + point;
		pointsLabel.setText(String.format("points: %d", points));
		pointsLabel.setSize(200, 42); // not a good practice
	}
}
