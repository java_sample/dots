package ee.itcollege.dots;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import ee.itcollege.dots.field.GameField;
import ee.itcollege.dots.field.TopPanel;

public class DotsGameStart implements Runnable {
	
	
	TopPanel top = new TopPanel();
	GameField game = new GameField(top);
	JFrame window = new JFrame("Dots!");
	
	@Override
	public void run() {
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        window.setLayout(new BorderLayout());
        window.add(top, BorderLayout.NORTH);
        
        window.add(game, BorderLayout.CENTER);
        window.setVisible(true);
        window.pack();
        
    	window.addKeyListener(new KeyAdapter() {
    		@Override
    		public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					System.exit(0);
				}
			}
		});
	}

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new DotsGameStart());
    }
}
