package ee.itcollege.dots.parts;

import java.awt.Color;
import java.awt.Graphics2D;

public class Dot {
	
	public static final int DOT_AREA_SIZE = 40;
	public static final int DOT_SIZE = 20;
	
	private static Color[] predefinedColors = {
			Color.red,
			Color.blue,
			Color.green,
			Color.yellow};
	
    private int x;
    private int y;
    private Color color;
    
    
    // konstruktor-meetod
    public Dot(int x, int y) {
    	this.x = x;
    	this.y = y;
    	
    	int randomIndex = (int) (Math.random() * predefinedColors.length);
    	color = predefinedColors[randomIndex];
	}
    
	public void drawItself(Graphics2D g) {
        g.setColor(color);
        g.fillOval(
        		x * DOT_AREA_SIZE + (DOT_AREA_SIZE - DOT_SIZE) / 2,
        		y * DOT_AREA_SIZE + (DOT_AREA_SIZE - DOT_SIZE) / 2,
        		DOT_SIZE,
        		DOT_SIZE);
    }
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void moveDown() {
		y++;
	}
	
	public Color getColor() {
		return color;
	}
	
	@Override
	public String toString() {
		return String.format("Dot(%d, %d)", x, y);
	}

}



