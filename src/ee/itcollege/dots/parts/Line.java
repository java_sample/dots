package ee.itcollege.dots.parts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

public class Line {
	
	private static final int SELECTION_SIZE = Dot.DOT_SIZE + 6; 

	private List<Dot> selectedDots = new ArrayList<Dot>();
	
	public void nextDot(Dot dot) {
		while (!selectedDots.isEmpty()) {
			Dot lastDot = selectedDots.get(selectedDots.size() - 1);
			
			// check that last and next have same color, otherwise return false;
			if (!lastDot.getColor().equals(dot.getColor())) {
				return;
			}
			
			if (canBeNext(lastDot, dot)) {
				break;
			}
			else {
				selectedDots.remove(lastDot);
			}
		}
		if (!selectedDots.contains(dot)) {
			selectedDots.add(dot);
		}
	}
	
	private boolean canBeNext(Dot last, Dot next) {
		
		int deltaX = Math.abs(last.getX() - next.getX());
		int deltaY = Math.abs(last.getY() - next.getY());
		
		return 1 == deltaX && 0 == deltaY || 0 == deltaX && 1 == deltaY;
	}
	
	public void clearSelection() {
		selectedDots.clear();
	}
	
	public List<Dot> getSelectedDots() {
		return selectedDots;
	}
	
	public void drawItself(Graphics2D g) {
		g.setColor(new Color(255, 0, 0, 80)); // rgba
		
		Dot lastDot = null;
		int size = Dot.DOT_AREA_SIZE;
		
		g.setStroke(new BasicStroke(4));
		for (Dot dot : selectedDots) {
			if (null != lastDot) {
				g.drawLine(
						lastDot.getX() * size + size / 2,
						lastDot.getY() * size + size / 2,
						dot.getX() * size + size / 2,
						dot.getY() * size + size / 2);
			}
			g.fillOval(
				dot.getX() * size + (size - SELECTION_SIZE) / 2,
				dot.getY() * size + (size - SELECTION_SIZE) / 2,
				SELECTION_SIZE,
				SELECTION_SIZE);
			lastDot = dot;
		}
	}

	
}
